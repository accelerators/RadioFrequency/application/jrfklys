package RFKLYS;

import RFTools.SeqHistoryPanel;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.INumberScalar;
import fr.esrf.tangoatk.core.IScalarAttribute;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKDiagnostic;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame implements ActionListener,SynopticProgressListener {

  private AttributeList attList;
  private CommandList cmdList;
  public static  ErrorHistory errWin;
  
  private int klysIdx;
  private String traName;
  private boolean runningFromShell;

  private JFileChooser fc = null;
  private String configFilePath = null;
  private String canonicalConfigFilePath = null;

  private Splash splash; 
  private StateStatusViewer stateStatusViewer;
  private StatusViewerFrame statusViewer = null;
  private SeqHistoryPanel historyPanel;
  
  private VoidVoidCommand offCmd = null;
  private VoidVoidCommand lowheatingCmd = null;
  private VoidVoidCommand standbyCmd = null;
  private VoidVoidCommand onCmd = null;
  private VoidVoidCommand stopCmd = null;
  private VoidVoidCommand resetCmd = null;
  
  private ICommand  readFileCmd  = null;
  private ICommand  writeFileCmd = null;
  private FileViewerFrame fileViewer = null;
  private RelayFrame relayFrame = null;
  
  private boolean useRelativePath = false;

  /**
   * Creates new form MainPanel
   */
   public MainPanel(String klysName) {
     this(klysName,false);
   }
 
   public MainPanel(String klysName,boolean runningFromShell) {
    
    // Check params
     
    if( klysName.equalsIgnoreCase("srrf1") ) {
      klysIdx = 0;
    } else if ( klysName.equalsIgnoreCase("srrf2") ) {
      klysIdx = 1;      
    } else {
      JOptionPane.showMessageDialog(this, "Invalid input argument: " + klysName + "\n" +
                                          "srrf1, srrf2 expected");
      System.exit(-1);
    }

    traName = RFKLYSConst.TRA_DEVNAME[klysIdx];

    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });

    // Init Swing stuff
    
    initComponents();
    
    // Init global variables
    
    this.runningFromShell = runningFromShell;
    
    fc = new JFileChooser();

    errWin = new ErrorHistory();
    
    attList = new AttributeList();
    attList.addErrorListener(errWin);
    cmdList = new CommandList();
    cmdList.addErrorListener(errWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());

    // Splash window
    
    splash = new Splash();
    splash.setTitle("RF Klystron " + RFKLYSConst.APP_RELEASE);
    splash.setCopyright("(c) ESRF 2013");
    splash.setMaxProgress(100);
    splash.progress(0);

    // Scalar Viewers
    
    frequencyViewer.setAlarmEnabled(false);
    frequencyViewer.setBackground(innerPanel.getBackground());
    phaseViewer.setAlarmEnabled(false);
    phaseViewer.setBackground(innerPanel.getBackground());
    ePowerViewer.setAlarmEnabled(false);
    ePowerViewer.setBackground(innerPanel.getBackground());
    rfPowerViewer.setAlarmEnabled(false);
    rfPowerViewer.setBackground(innerPanel.getBackground());
    transModeViewer.setAlarmEnabled(false);
    transModeViewer.setBackground(innerPanel.getBackground());
    configFileViewer.setAlarmEnabled(false);
    configFileViewer.setBackground(innerPanel.getBackground());
    
    stateStatusViewer = new StateStatusViewer();
    stateStatusPanel.add(stateStatusViewer,BorderLayout.CENTER);
    
    splash.progress(5);
    
    historyPanel = new SeqHistoryPanel(this,errWin,traName);
    mainSplitPane.setRightComponent(null);

    splash.progress(10);
    initScalarViewer(frequencyViewer,RFKLYSConst.MS_DEVNAME+"/frequency");
    splash.progress(11);
    initScalarViewer(phaseViewer,traName+"/Phase");
    splash.progress(12);
    initScalarViewer(ePowerViewer,traName+"/Elec_Power");
    splash.progress(13);
    initScalarViewer(rfPowerViewer,traName+"/RF_Power");
    splash.progress(14);
    initScalarViewer(transModeViewer,traName+"/Transmitter_mode");
    splash.progress(15);
    initScalarViewer(configFileViewer,traName+"/Configuration_File");
    splash.progress(16);
    initStateStatusViewer(stateStatusViewer,traName+"/State",traName+"/ShortStatus");
    splash.progress(17);
                      
   
    // Init configuration file path stuff
    
    try {
    
      DeviceProxy ds = new DeviceProxy(traName);
      DeviceData dd = ds.command_inout("GetConfigurationFilePath");
      configFilePath = dd.extractString();
      
      // The following line is added to work around a Bug appeared since Java 1.5.
      // Bug description : When a mounting point is used through a symbolic link in a file system
      //    (for example /operation -->link--> /_mntdirect/operation (which is the real mounting point)
      //  The first time the "FileChooser.setCurrentDirectory(new File(serverDirPath)) is called the path returned through
      //  filechooser.getSelelectedFile().getAbsolutePath() or getPath() is the path with the real mounting point
      //  where the second call to FileChooser.setCurrentDirectory(new File(serverDirPath)) will make the
      //  filechooser.getSelelectedFile().getAbsolutePath() return the path using the symbolique link. To avoid this
      //  inconsistency we should save the ServerDirPath with the symbolic link resolved (not containing any symb. link)
      //  by calling getCanonicalPath();
      //  call to fc.setCurrentDirectory(new File(serverDirPath));
      
      File sDir = new File(configFilePath);
      try {
        canonicalConfigFilePath = sDir.getCanonicalPath();
      } catch (IOException ioex) {
    	  canonicalConfigFilePath = configFilePath;
      }
      
    } catch( DevFailed e ) {
      splash.setVisible(false);
      ErrorPane.showErrorMessage(this, traName, e);
      
    }
            
    try {
      
      if( klysIdx==0 || klysIdx==1 ) {
        
        // SRRF1 & SRRF2 with new sequencer
        useRelativePath = true;
        readFileCmd = (ICommand)cmdList.add(traName+"/LoadConfigurationFile");
        splash.progress(19);
        writeFileCmd = (ICommand)cmdList.add(traName+"/SaveConfigurationFile");
        splash.progress(20);
        
      } else {
        readFileCmd = (ICommand)cmdList.add(traName+"/ReadConfigurationFile");
        splash.progress(19);
        writeFileCmd = (ICommand)cmdList.add(traName+"/WriteConfigurationFile");
        splash.progress(20);
      }

    } catch( ConnectionException e) {
    }

            
    // Command Viewer

    splash.progress(20);
    
    try {
      
      offCmd = (VoidVoidCommand)cmdList.add(traName+"/Off");
      splash.progress(21);
      lowheatingCmd = (VoidVoidCommand)cmdList.add(traName+"/LowHeating");
      splash.progress(22);
      standbyCmd = (VoidVoidCommand)cmdList.add(traName+"/Standby");
      splash.progress(23);
      onCmd = (VoidVoidCommand)cmdList.add(traName+"/On");
      splash.progress(24);
      stopCmd = (VoidVoidCommand)cmdList.add(traName+"/Stop");
      splash.progress(25);
      resetCmd = (VoidVoidCommand)cmdList.add(traName+"/Reset");
      splash.progress(26);

    } catch( ConnectionException e) {
    }

    // Loads the synoptic
    
    theSynoptic.setProgressListener(this);
    
    InputStream jdFileInStream = this.getClass().getResourceAsStream(RFKLYSConst.SYNOPTIC_FILE_NAME[klysIdx]);

    if (jdFileInStream == null) {
      splash.setVisible(false);
      JOptionPane.showMessageDialog(
              null, "Failed to get the inputStream for the synoptic file resource : " + RFKLYSConst.SYNOPTIC_FILE_NAME[klysIdx] + " \n\n",
              "Resource error",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }

    InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
    try {
      theSynoptic.setErrorHistoryWindow(errWin);
      theSynoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
      theSynoptic.setAutoZoom(true);
      theSynoptic.loadSynopticFromStream(inStrReader);
    } catch (IOException ioex) {
      splash.setVisible(false);
      JOptionPane.showMessageDialog(
              null, "Cannot find load the synoptic input stream reader.\n" + " Application will abort ...\n" + ioex,
              "Resource access failed",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }
    
    
    attList.setRefreshInterval(1000);
    attList.startRefresher();
    
    splash.setVisible(false);
    setTitle("RF Klystron " + RFKLYSConst.APP_RELEASE + " [" + traName + " ]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
        
  }

  public void progress(double p) {
    splash.progress(40+(int)(p*60.0));
  }

  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      setVisible(false);
      dispose();

    }

  }

  /**
   * Clear all connections to attributes and commands
   */
  private void clearModel() {

    theSynoptic.clearSynopticFileModel();

    frequencyViewer.clearModel();
    phaseViewer.clearModel();
    ePowerViewer.clearModel();
    rfPowerViewer.clearModel();
    transModeViewer.clearModel();
    configFileViewer.clearModel();
    stateStatusViewer.clearModel();
    historyPanel.clearModel();
               
    attList.removeErrorListener(errWin);
    attList.stopRefresher();
    attList.clear();
    
    cmdList.removeErrorListener(errWin);
    cmdList.removeErrorListener(ErrorPopup.getInstance());
    cmdList.clear();
    
    if( statusViewer!=null ) statusViewer.clearModel();

  }

  private void initScalarViewer(SimpleScalarViewer viewer,String attName) {

    try {
      viewer.setText(viewer.getInvalidText());
      IScalarAttribute model = (IScalarAttribute)attList.add(attName);
      if( model instanceof IStringScalar ) {
        viewer.setModel((IStringScalar)model);
      } else if ( model instanceof INumberScalar ) {
        viewer.setModel((INumberScalar)model);
      } else if ( model instanceof IBooleanScalar ) {
        viewer.setModel((IBooleanScalar)model);
      } else {
        splash.setVisible(false);
        JOptionPane.showMessageDialog(this, "Wrong attribute type for " + attName);
      }
      
    } catch( ConnectionException e) {
      // Error message goes automatically to error window
    }

  }
  
  private void initStateStatusViewer(StateStatusViewer viewer,String attStateName,String attStatusName) {

    try {
      
      viewer.setText("UNKNOWN");      
      IStringScalar statusModel = (IStringScalar)attList.add(attStatusName);
      viewer.setStatusModel(statusModel);
      IDevStateScalar stateModel = (IDevStateScalar)attList.add(attStateName);
      viewer.setStateModel(stateModel);
            
    } catch( ConnectionException e) {
      // Error message goes automatically to error window
    }

  }

  public void hideSeqHistory() {
    mainSplitPane.setRightComponent(null);
    pack();    
  }
  
  public void showSeqHistory() {
    mainSplitPane.setRightComponent(historyPanel);
    pack();    
  }
  
  private File pickAFileName(String action) {
    
    int dialReturn;
    File selectedFile = null;
    String selectedFilePath = null;
    int serverPathLength = 0;

    if (configFilePath == null) {
      JOptionPane.showMessageDialog(
              null, "Cannot get the config file root directory.\n" + "Check the " + traName + " device is running and responding correctly to reads on ConfigurationFilePath attribute.\n",
              action + " file aborted.\n",
              JOptionPane.ERROR_MESSAGE);
      return null;
    }

    serverPathLength = canonicalConfigFilePath.length();

    if (serverPathLength <= 0) {
      JOptionPane.showMessageDialog(
              null, "The config file root directory is not set correctly.\n" + "Check the " + traName + " device is running and responding correctly reads on ConfigurationFilePath attribute.\n",
              action + " file aborted.\n",
              JOptionPane.ERROR_MESSAGE);
      return null;
    }

    fc.setCurrentDirectory(new File(configFilePath));
    dialReturn = fc.showDialog(this, action);

    if (dialReturn == JFileChooser.APPROVE_OPTION) {
      selectedFile = fc.getSelectedFile();
      try {
        selectedFilePath = selectedFile.getCanonicalPath();
      } catch (IOException ioex) {
        javax.swing.JOptionPane.showMessageDialog(
                null, "Failed to get the canonical path of the selected file.\n\n" + ioex + "\n\n\n",
                action + " file aborted.\n",
                javax.swing.JOptionPane.ERROR_MESSAGE);
        return null;
      }

      
      if (selectedFilePath.startsWith(canonicalConfigFilePath) != true) {
        javax.swing.JOptionPane.showMessageDialog(
                null, "The selected file is not inside the authorized root directory.\n\n" + "The file should be located in " + canonicalConfigFilePath + " directory tree.\n\n",
                action + " file aborted.\n",
                javax.swing.JOptionPane.ERROR_MESSAGE);
        return null;
      }
      
      return selectedFile;
    } else {
      return null;
    }
    
  }
  
  private boolean confirmCommand(String command) {
    
    if( command.equalsIgnoreCase("On") ) 
      return true;
    
    String state = stateStatusViewer.getState();
    String mode = transModeViewer.getText();
    
    if (state.equalsIgnoreCase(IDevice.UNKNOWN)) {
      int r = JOptionPane.showConfirmDialog(this,
              "WARNING, cannot get RF transmitter state.\n Do you want to execute '" + command + "' command ?",
              "Confirmation",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      return (r == JOptionPane.YES_OPTION);
    }

    if (mode.equalsIgnoreCase(transModeViewer.getInvalidText())) {
      int r = JOptionPane.showConfirmDialog(this,
              "WARNING, cannot get the transmitter mode.\n Do you want to execute '" + command + "' command ?",
              "Confirmation",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);
      return (r == JOptionPane.YES_OPTION);
    }
    
    if( state.equalsIgnoreCase(IDevice.ON) || state.equalsIgnoreCase(IDevice.ALARM) ) {      
      if( mode.equalsIgnoreCase("2 CAVITIES") || mode.equalsIgnoreCase("4 CAVITIES") ) {
        int r = JOptionPane.showConfirmDialog(this,
                "WARNING, this transmitter is connected  on the SR.\n Do you want to execute '" + command + "' command ?",
                "Confirmation",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        return (r == JOptionPane.YES_OPTION);
      }      
    }    
    
    return true;
    
  }


  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    mainSplitPane = new javax.swing.JSplitPane();
    innerPanel = new javax.swing.JPanel();
    upPanel = new javax.swing.JPanel();
    frequencyLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    frequencyViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    phaseLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    phaseViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    ePowerLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    ePowerViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    rfPowerLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    rfPowerViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    transModeLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    transModeViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    synopticPanel = new javax.swing.JPanel();
    theSynoptic = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
    downPanel = new javax.swing.JPanel();
    cmdPanel = new javax.swing.JPanel();
    offButton = new javax.swing.JButton();
    lowheatingButton = new javax.swing.JButton();
    standbyButton = new javax.swing.JButton();
    onButtonButton = new javax.swing.JButton();
    Reset = new javax.swing.JButton();
    stateStatusPanel = new javax.swing.JPanel();
    configFileLabel = new fr.esrf.tangoatk.widget.util.JSmoothLabel();
    configFileViewer = new fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer();
    dummyHistoryPanel = new javax.swing.JPanel();
    jMenuBar1 = new javax.swing.JMenuBar();
    jMenuFile = new javax.swing.JMenu();
    readConfigMenuItem = new javax.swing.JMenuItem();
    saveConfigMenuItem = new javax.swing.JMenuItem();
    previewConfigMenuItem = new javax.swing.JMenuItem();
    deleteConfigMenuItem = new javax.swing.JMenuItem();
    exitMenuItem = new javax.swing.JMenuItem();
    jMenuView = new javax.swing.JMenu();
    viewStatusMenuItem = new javax.swing.JMenuItem();
    viewErrorMenuItem = new javax.swing.JMenuItem();
    viewDiagMenuItem = new javax.swing.JMenuItem();
    jSeparator1 = new javax.swing.JPopupMenu.Separator();
    viewHisotryMenuItem = new javax.swing.JMenuItem();
    jMenuApplication = new javax.swing.JMenu();
    tuningKlystronMenuItem = new javax.swing.JMenuItem();
    tuningOperationMenuItem = new javax.swing.JMenuItem();
    tuningLoopMenuItem = new javax.swing.JMenuItem();
    tuningCounterMenuItem = new javax.swing.JMenuItem();
    conditioningMenuItem = new javax.swing.JMenuItem();

    mainSplitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

    innerPanel.setLayout(new java.awt.BorderLayout());

    java.awt.GridBagLayout upPanelLayout = new java.awt.GridBagLayout();
    upPanelLayout.columnWeights = new double[] {1.0, 1.0, 1.0, 1.0, 1.0};
    upPanel.setLayout(upPanelLayout);

    frequencyLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    frequencyLabel.setOpaque(false);
    frequencyLabel.setText("Frequency");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(frequencyLabel, gridBagConstraints);

    frequencyViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    frequencyViewer.setText("simpleScalarViewer1");
    frequencyViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    frequencyViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
    upPanel.add(frequencyViewer, gridBagConstraints);

    phaseLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    phaseLabel.setOpaque(false);
    phaseLabel.setText("Phase");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(phaseLabel, gridBagConstraints);

    phaseViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    phaseViewer.setText("simpleScalarViewer1");
    phaseViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    phaseViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
    upPanel.add(phaseViewer, gridBagConstraints);

    ePowerLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    ePowerLabel.setOpaque(false);
    ePowerLabel.setText("Elec. Power");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(ePowerLabel, gridBagConstraints);

    ePowerViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    ePowerViewer.setText("simpleScalarViewer1");
    ePowerViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    ePowerViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
    upPanel.add(ePowerViewer, gridBagConstraints);

    rfPowerLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    rfPowerLabel.setOpaque(false);
    rfPowerLabel.setText("RF Power");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(rfPowerLabel, gridBagConstraints);

    rfPowerViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    rfPowerViewer.setText("simpleScalarViewer1");
    rfPowerViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    rfPowerViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
    upPanel.add(rfPowerViewer, gridBagConstraints);

    transModeLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    transModeLabel.setOpaque(false);
    transModeLabel.setText("Transmitter Mode");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 4;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    upPanel.add(transModeLabel, gridBagConstraints);

    transModeViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    transModeViewer.setText("simpleScalarViewer1");
    transModeViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    transModeViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    transModeViewer.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        transModeViewerActionPerformed(evt);
      }
    });
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 4;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
    upPanel.add(transModeViewer, gridBagConstraints);

    innerPanel.add(upPanel, java.awt.BorderLayout.NORTH);

    synopticPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    synopticPanel.setLayout(new java.awt.BorderLayout());

    theSynoptic.setAutoZoom(true);
    synopticPanel.add(theSynoptic, java.awt.BorderLayout.CENTER);

    innerPanel.add(synopticPanel, java.awt.BorderLayout.CENTER);

    java.awt.GridBagLayout downPanelLayout = new java.awt.GridBagLayout();
    downPanelLayout.columnWeights = new double[] {0.0, 1.0};
    downPanel.setLayout(downPanelLayout);

    cmdPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

    offButton.setText("Off");
    offButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        offButtonActionPerformed(evt);
      }
    });
    cmdPanel.add(offButton);

    lowheatingButton.setText("Low Heating");
    lowheatingButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        lowheatingButtonActionPerformed(evt);
      }
    });
    cmdPanel.add(lowheatingButton);

    standbyButton.setText("Standby");
    standbyButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        standbyButtonActionPerformed(evt);
      }
    });
    cmdPanel.add(standbyButton);

    onButtonButton.setText("On");
    onButtonButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        onButtonButtonActionPerformed(evt);
      }
    });
    cmdPanel.add(onButtonButton);

    Reset.setLabel("Reset");
    Reset.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        ResetActionPerformed(evt);
      }
    });
    cmdPanel.add(Reset);

    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    downPanel.add(cmdPanel, gridBagConstraints);

    stateStatusPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    stateStatusPanel.setLayout(new java.awt.BorderLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.insets = new java.awt.Insets(2, 5, 0, 5);
    downPanel.add(stateStatusPanel, gridBagConstraints);

    configFileLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    configFileLabel.setOpaque(false);
    configFileLabel.setText("Configuration File");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    gridBagConstraints.insets = new java.awt.Insets(2, 0, 0, 0);
    downPanel.add(configFileLabel, gridBagConstraints);

    configFileViewer.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    configFileViewer.setText("simpleScalarViewer1");
    configFileViewer.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
    configFileViewer.setMargin(new java.awt.Insets(3, 0, 3, 0));
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 1;
    gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
    downPanel.add(configFileViewer, gridBagConstraints);

    innerPanel.add(downPanel, java.awt.BorderLayout.PAGE_END);

    mainSplitPane.setTopComponent(innerPanel);

    dummyHistoryPanel.setPreferredSize(new java.awt.Dimension(0, 0));
    dummyHistoryPanel.setLayout(new java.awt.BorderLayout());
    mainSplitPane.setBottomComponent(dummyHistoryPanel);

    getContentPane().add(mainSplitPane, java.awt.BorderLayout.CENTER);

    jMenuFile.setText("File");

    readConfigMenuItem.setText("Read configuration ...");
    readConfigMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        readConfigMenuItemActionPerformed(evt);
      }
    });
    jMenuFile.add(readConfigMenuItem);

    saveConfigMenuItem.setText("Save configuration ...");
    saveConfigMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        saveConfigMenuItemActionPerformed(evt);
      }
    });
    jMenuFile.add(saveConfigMenuItem);

    previewConfigMenuItem.setText("Preview configuration ...");
    previewConfigMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        previewConfigMenuItemActionPerformed(evt);
      }
    });
    jMenuFile.add(previewConfigMenuItem);

    deleteConfigMenuItem.setText("Delete configuration ...");
    deleteConfigMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        deleteConfigMenuItemActionPerformed(evt);
      }
    });
    jMenuFile.add(deleteConfigMenuItem);

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    jMenuFile.add(exitMenuItem);

    jMenuBar1.add(jMenuFile);

    jMenuView.setText("View");

    viewStatusMenuItem.setText("Status ...");
    viewStatusMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewStatusMenuItemActionPerformed(evt);
      }
    });
    jMenuView.add(viewStatusMenuItem);

    viewErrorMenuItem.setText("Errors ...");
    viewErrorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewErrorMenuItemActionPerformed(evt);
      }
    });
    jMenuView.add(viewErrorMenuItem);

    viewDiagMenuItem.setText("Diagnostics ...");
    viewDiagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewDiagMenuItemActionPerformed(evt);
      }
    });
    jMenuView.add(viewDiagMenuItem);
    jMenuView.add(jSeparator1);

    viewHisotryMenuItem.setText("History");
    viewHisotryMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        viewHisotryMenuItemActionPerformed(evt);
      }
    });
    jMenuView.add(viewHisotryMenuItem);

    jMenuBar1.add(jMenuView);

    jMenuApplication.setText("Applications");

    tuningKlystronMenuItem.setText("Tuning Klystron");
    tuningKlystronMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tuningKlystronMenuItemActionPerformed(evt);
      }
    });
    jMenuApplication.add(tuningKlystronMenuItem);

    tuningOperationMenuItem.setText("Tuning Operation");
    tuningOperationMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tuningOperationMenuItemActionPerformed(evt);
      }
    });
    jMenuApplication.add(tuningOperationMenuItem);

    tuningLoopMenuItem.setText("Tuning Loop");
    tuningLoopMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tuningLoopMenuItemActionPerformed(evt);
      }
    });
    jMenuApplication.add(tuningLoopMenuItem);

    tuningCounterMenuItem.setText("Counters");
    tuningCounterMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tuningCounterMenuItemActionPerformed(evt);
      }
    });
    jMenuApplication.add(tuningCounterMenuItem);

    conditioningMenuItem.setText("Conditiong...");
    conditioningMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        conditioningMenuItemActionPerformed(evt);
      }
    });
    jMenuApplication.add(conditioningMenuItem);

    jMenuBar1.add(jMenuApplication);

    setJMenuBar(jMenuBar1);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
    // TODO add your handling code here:
    exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void viewErrorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewErrorMenuItemActionPerformed
    // TODO add your handling code here:
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);    
  }//GEN-LAST:event_viewErrorMenuItemActionPerformed

  private void viewDiagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewDiagMenuItemActionPerformed
    // TODO add your handling code here:
    ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_viewDiagMenuItemActionPerformed

  private void viewStatusMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewStatusMenuItemActionPerformed
    // TODO add your handling code here:
    if(statusViewer==null) statusViewer=new StatusViewerFrame(traName);
    statusViewer.showFrame();
  }//GEN-LAST:event_viewStatusMenuItemActionPerformed

  private void viewHisotryMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewHisotryMenuItemActionPerformed
    // TODO add your handling code here:
    showSeqHistory();
  }//GEN-LAST:event_viewHisotryMenuItemActionPerformed

  private void previewConfigMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_previewConfigMenuItemActionPerformed
    // TODO add your handling code here:
    File selectedFile = null;

    selectedFile = pickAFileName("View");
    if (selectedFile == null) {
      return;
    }
    if (selectedFile.exists()) {
      if(fileViewer==null) fileViewer=new FileViewerFrame();
      fileViewer.displayFileContent(selectedFile);
      ATKGraphicsUtils.centerFrame(synopticPanel, fileViewer);
      fileViewer.setVisible(true);
    } else {
      JOptionPane.showMessageDialog(this, "The File '" + selectedFile.getAbsolutePath() + "' does not exists !");
    }

  }//GEN-LAST:event_previewConfigMenuItemActionPerformed

  private void readConfigMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_readConfigMenuItemActionPerformed
    // TODO add your handling code here:
    File selectedFile = null;

    selectedFile = pickAFileName("Read");
    if (selectedFile == null) {
      return;
    }

    if (selectedFile.exists()) {      
      // The VM see the NFS as /mnt/_operation ...
      // but the TACO server see the NFS as /operation ...
      String corrected = selectedFile.getAbsolutePath();
      if( useRelativePath ) {
        corrected = corrected.replace((CharSequence) canonicalConfigFilePath, (CharSequence)"");        
        corrected = corrected.replace((CharSequence) configFilePath, (CharSequence)"");        
        if(corrected.startsWith("/")) corrected = corrected.substring(1);
      } else {
        corrected = corrected.replace((CharSequence) canonicalConfigFilePath, (CharSequence) configFilePath);
      }
      // Replace // occurence with /. It can occurs if the property Configuration_file_path on the
      // transmitter TANGO device contains a terminal /.
      corrected = corrected.replace((CharSequence) "//", (CharSequence) "/");
      Vector<String> inputArg = new Vector<String>();
      inputArg.add(corrected);      
      readFileCmd.execute(inputArg);
    } else {
      JOptionPane.showMessageDialog(this, "The File '" + selectedFile.getAbsolutePath() + "' does not exists !");
    }
    
  }//GEN-LAST:event_readConfigMenuItemActionPerformed

  private void saveConfigMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveConfigMenuItemActionPerformed
    // TODO add your handling code here:
    File selectedFile = null;

    selectedFile = pickAFileName("Write");
    if (selectedFile == null) {
      return;
    }

    // The VM see the NFS as /mnt/_operation ...
    // but the TACO server see the NFS as /operation ...
    String corrected = selectedFile.getAbsolutePath();
    if( useRelativePath ) {
      corrected = corrected.replace((CharSequence) canonicalConfigFilePath, (CharSequence)"");        
      corrected = corrected.replace((CharSequence) configFilePath, (CharSequence)"");
      if(corrected.startsWith("/")) corrected = corrected.substring(1);
    } else {
      corrected = corrected.replace((CharSequence) canonicalConfigFilePath, (CharSequence) configFilePath);
    }
    // Replace // occurence with /. It can occurs if the property Configuration_file_path on the
    // transmitter TANGO device contains a terminal /.
    corrected = corrected.replace((CharSequence) "//", (CharSequence) "/");
    Vector<String> inputArg = new Vector<String>();
    inputArg.add(corrected);
    writeFileCmd.execute(inputArg);
    
  }//GEN-LAST:event_saveConfigMenuItemActionPerformed

  private void deleteConfigMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteConfigMenuItemActionPerformed
    // TODO add your handling code here:
    File selectedFile = null;

    selectedFile = pickAFileName("Delete");
    if (selectedFile == null) {
      return;
    }
    if (selectedFile.exists()) {
      int r = JOptionPane.showConfirmDialog(this,
              "Do you really want to delete the file : '" + selectedFile.getAbsolutePath() + "' ?",
              "Confirmation",
              JOptionPane.YES_NO_OPTION,
              JOptionPane.WARNING_MESSAGE);

      if (r == JOptionPane.YES_OPTION) {
        boolean ret = selectedFile.delete();
        if (ret) {
          JOptionPane.showMessageDialog(this, "File successfully deleted : " + selectedFile.getAbsolutePath());
        } else {
          JOptionPane.showMessageDialog(this, "Cannot delete file : " + selectedFile.getAbsolutePath());
        }
      }
    }
  }//GEN-LAST:event_deleteConfigMenuItemActionPerformed

  private void tuningKlystronMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tuningKlystronMenuItemActionPerformed
    // TODO add your handling code here:
    String panel = RFKLYSConst.TUNNING_KLYSTRON_PARAMS[klysIdx];
    new atktuning.MainPanel(panel, false, false, false, true);      
  }//GEN-LAST:event_tuningKlystronMenuItemActionPerformed

  private void tuningOperationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tuningOperationMenuItemActionPerformed
    // TODO add your handling code here:
    String panel = RFKLYSConst.TUNNING_OPERATION_PARAMS[klysIdx];
    new atktuning.MainPanel(panel, false, false, false, true);      
  }//GEN-LAST:event_tuningOperationMenuItemActionPerformed

  private void tuningLoopMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tuningLoopMenuItemActionPerformed
    // TODO add your handling code here:
    String panel = RFKLYSConst.TUNNING_LOOP_PARAMS[klysIdx];
    new atktuning.MainPanel(panel, false, false, false, true);      
  }//GEN-LAST:event_tuningLoopMenuItemActionPerformed

  private void offButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_offButtonActionPerformed
    // TODO add your handling code here:
    if(confirmCommand("Off")) {
      if (stateStatusViewer.getState().equalsIgnoreCase(IDevice.DISABLE))
         // Send DevStop on the TACO Device (Reset the sequencer)
         stopCmd.execute();
      offCmd.execute();
    }
  }//GEN-LAST:event_offButtonActionPerformed

  private void lowheatingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lowheatingButtonActionPerformed
    // TODO add your handling code here:
    if(confirmCommand("LowHeating")) {
      if (stateStatusViewer.getState().equalsIgnoreCase(IDevice.DISABLE))
         // Send DevStop on the TACO Device (Reset the sequencer)
         stopCmd.execute();
      lowheatingCmd.execute();
    }
  }//GEN-LAST:event_lowheatingButtonActionPerformed

  private void standbyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_standbyButtonActionPerformed
    // TODO add your handling code here:
    if(confirmCommand("Standby")) {
      if (stateStatusViewer.getState().equalsIgnoreCase(IDevice.DISABLE))
         // Send DevStop on the TACO Device (Reset the sequencer)
         stopCmd.execute();
      standbyCmd.execute();
    }
  }//GEN-LAST:event_standbyButtonActionPerformed

  private void onButtonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onButtonButtonActionPerformed
    // TODO add your handling code here:
    if(confirmCommand("On")) {
      if (stateStatusViewer.getState().equalsIgnoreCase(IDevice.DISABLE))
         // Send DevStop on the TACO Device (Reset the sequencer)
         stopCmd.execute();
      onCmd.execute();
    }
  }//GEN-LAST:event_onButtonButtonActionPerformed

  private void ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ResetActionPerformed
    // TODO add your handling code here:
    resetCmd.execute();
  }//GEN-LAST:event_ResetActionPerformed

  private void tuningCounterMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tuningCounterMenuItemActionPerformed
    String panel = RFKLYSConst.TUNNING_COUNTERS[klysIdx];
    new atktuning.MainPanel(panel, false, false, false, true);      
  }//GEN-LAST:event_tuningCounterMenuItemActionPerformed

  private void conditioningMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_conditioningMenuItemActionPerformed
    if( relayFrame==null ) 
      relayFrame = new RelayFrame(klysIdx);
    ATKGraphicsUtils.centerFrame(this.innerPanel, relayFrame);    
    relayFrame.setVisible(true);
  }//GEN-LAST:event_conditioningMenuItemActionPerformed

  private void transModeViewerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transModeViewerActionPerformed
    // TODO add your handling code here:
  }//GEN-LAST:event_transModeViewerActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    if(args.length!=1) {
      System.out.println("This application expects one instance parameter on the command line:");
      System.out.println("    parameter1: instance (SRRF1, SRRF2 or SRRF3)");      
    } else {
      /* Create and display the form */
      new MainPanel(args[0],true);
    }
    
  }
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton Reset;
  private javax.swing.JPanel cmdPanel;
  private javax.swing.JMenuItem conditioningMenuItem;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel configFileLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer configFileViewer;
  private javax.swing.JMenuItem deleteConfigMenuItem;
  private javax.swing.JPanel downPanel;
  private javax.swing.JPanel dummyHistoryPanel;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel ePowerLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer ePowerViewer;
  private javax.swing.JMenuItem exitMenuItem;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel frequencyLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer frequencyViewer;
  private javax.swing.JPanel innerPanel;
  private javax.swing.JMenu jMenuApplication;
  private javax.swing.JMenuBar jMenuBar1;
  private javax.swing.JMenu jMenuFile;
  private javax.swing.JMenu jMenuView;
  private javax.swing.JPopupMenu.Separator jSeparator1;
  private javax.swing.JButton lowheatingButton;
  private javax.swing.JSplitPane mainSplitPane;
  private javax.swing.JButton offButton;
  private javax.swing.JButton onButtonButton;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel phaseLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer phaseViewer;
  private javax.swing.JMenuItem previewConfigMenuItem;
  private javax.swing.JMenuItem readConfigMenuItem;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel rfPowerLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer rfPowerViewer;
  private javax.swing.JMenuItem saveConfigMenuItem;
  private javax.swing.JButton standbyButton;
  private javax.swing.JPanel stateStatusPanel;
  private javax.swing.JPanel synopticPanel;
  private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer theSynoptic;
  private fr.esrf.tangoatk.widget.util.JSmoothLabel transModeLabel;
  private fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer transModeViewer;
  private javax.swing.JMenuItem tuningCounterMenuItem;
  private javax.swing.JMenuItem tuningKlystronMenuItem;
  private javax.swing.JMenuItem tuningLoopMenuItem;
  private javax.swing.JMenuItem tuningOperationMenuItem;
  private javax.swing.JPanel upPanel;
  private javax.swing.JMenuItem viewDiagMenuItem;
  private javax.swing.JMenuItem viewErrorMenuItem;
  private javax.swing.JMenuItem viewHisotryMenuItem;
  private javax.swing.JMenuItem viewStatusMenuItem;
  // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        if( e.getSource()==historyPanel ) {
            if( e.getActionCommand()=="ShowSeqHistory") {
              showSeqHistory();
            } else if ( e.getActionCommand()=="HideSeqHistory") {
              hideSeqHistory();                
            }
        }
    }
}
