package RFKLYS;

/**
 *
 * @author pons
 */
public class RFKLYSConst {
  
  static public final String APP_RELEASE = "5.4";
          
  static public final String MS_DEVNAME="sy/ms/1";
  
  static public final String[] SYNOPTIC_FILE_NAME = {
    "/RFKLYS/srrf1_klyst.jdw",
    "/RFKLYS/srrf2_klyst.jdw"
  };

  static public final String[] PLC_SYNOPTIC_FILE_NAME = {
    "/RFKLYS/srrf1_plc.jdw",
    "/RFKLYS/srrf2_plc.jdw"
  };

  static public final String[][] PLC_NAMES = {
    {"srrf/plc/tra1-1","srrf/plc/tra1-2","srrf/cav-wg/load","srrf/his/tra1-psw","srrf/his/tra1-crwb","srrf/his/tra1-arcs"},
    {"srrf/plc/tra2-1","srrf/plc/tra2-2","srrf/cav-wg/load","srrf/his/tra2-psw","srrf/his/tra2-crwb","srrf/his/tra2-arcs"}
  };
  
  static public final String[] TRA_DEVNAME = {
    "srrf/tra/tra1",
    "srrf/tra/tra2"
  };
  
  static public final String[] TUNNING_KLYSTRON_PARAMS = {
     "srrf1-klys" ,
     "srrf2-klys"
  };
  
  static public final String[] TUNNING_OPERATION_PARAMS = {
    "srrf1-oper",
    "srrf2-oper"    
  };
  
  static public final String[] TUNNING_LOOP_PARAMS = {
    "srrf1-loop",
    "srrf2-loop"
  };  
  
  static public final String[] TUNNING_COUNTERS = {
    "srrf1-time",
    "srrf2-time"    
  };  
  
}
