/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class FilamentTangoPanel extends PanelCore {
  
  public FilamentTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("On","On");
    addCommand("Standby","Standby");
    addCommand("Off","Off");
    addCommand("Reset","Reset");
            
    display();
                        
  }  

}
