package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class TangoPanel extends PanelCore {
  
  public TangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("On","On");
    addCommand("Off","Off");
    addCommand("Reset","Reset");
                        
    display();

  }  
  
}
