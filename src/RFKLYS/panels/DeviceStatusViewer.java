/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.StatusEvent;
import java.awt.Color;
import java.awt.Insets;
import javax.swing.JTextArea;

/**
 *
 * @author pons
 */
public class DeviceStatusViewer extends JTextArea implements fr.esrf.tangoatk.core.IStatusListener {

  IDevice device = null;

  public DeviceStatusViewer() {
    
    setEditable(false);
    setMargin(new Insets(5,5,5,5));
    setBackground(new Color(238,238,238));
    
  }
         
  public void setModel(Device device) {
 
   
    if (this.device != null)
      this.device.removeStatusListener(this);
    setText("");
    this.device = device;
    if( device!=null ) {
      device.addStatusListener(this);
      setStatus(device.getStatus());
      setCaretPosition(0);
    }

  }

  public void setStatus(String s) {
    if (s.equals(getText()))
      return;
    else
      setText(s);
  }

  public void statusChange(StatusEvent evt) {
    setStatus(evt.getStatus());
    setCaretPosition(0);
  }

  
}
