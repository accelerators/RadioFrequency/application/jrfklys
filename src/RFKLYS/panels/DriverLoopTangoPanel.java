/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class DriverLoopTangoPanel extends PanelCore {
  
  public DriverLoopTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("Open","Open");
    addCommand("Close","Close");
    addCommand("Reset","Reset");
            
    addMode("SetModePin","Power In");
    addMode("SetModePout","Power Out");
            
    display();
                        
  }  

}
