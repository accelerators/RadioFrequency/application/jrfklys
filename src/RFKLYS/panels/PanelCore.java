/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

import RFKLYS.MainPanel;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.IAttribute;
import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEntityFilter;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.ScalarListViewer;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 *
 * @author pons
 */
public class PanelCore extends javax.swing.JFrame {

  private Device device;
  private DeviceStatusViewer statusViewer;
  private boolean initOK;
  private Vector<VoidVoidCommand> commands;
  private Vector<VoidVoidCommand> modes;
  private CommandList cmdList;
  private int cmdPos;
  private int modePos;
  private AttributeList attList = null;
  private ScalarListViewer sl = null;
  private JScrollPane sp = null;

  /**
   * Creates new form TacoPanel
   */
    
  public PanelCore(String devName) {
    this(devName,false);
  }
  
  public PanelCore(String devName,boolean showPanel) {

    initOK = false;

    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
         exitForm();
      }
    });
    
    commands = new Vector<VoidVoidCommand>();
    modes = new Vector<VoidVoidCommand>();
    cmdList = new CommandList();
    cmdList.addErrorListener(ErrorPopup.getInstance());
    
    initComponents();
    
    try {
      device = DeviceFactory.getInstance().getDevice(devName);
    } catch (ConnectionException ex) {
      ErrorPane.showErrorMessage(this, devName, ex);
      return;
    }
    
    statusViewer = new DeviceStatusViewer();
    JScrollPane scrollPane = new JScrollPane(statusViewer);
    scrollPane.setBorder( BorderFactory.createTitledBorder(device.getName()));
    scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    statusPanel.add(scrollPane,BorderLayout.CENTER);
    statusViewer.setModel(device);
    
    // Adjust size
    pack();
    Dimension s = scrollPane.getSize();
    if(s.width<300)
      s.width = 300;
    if(s.height<120)
      s.height = 120;
    scrollPane.getViewport().setPreferredSize(s);
    
    
    initOK = true;
    cmdPos = 0;
    modePos = 0;
    splitPane.setRightComponent(null);
    
    panelButton.setVisible(showPanel);
    panelButton.setText("Show panel");
            
  }
  
  public void addPanel() {
    
    if( attList==null) {
      
      attList = new AttributeList();
      attList.setFilter(new IEntityFilter() {

        @Override
        public boolean keep(IEntity ie) {
          
          if( !(ie instanceof IAttribute) )
            return false;
          IAttribute a = (IAttribute)ie;
          if( !a.isScalar() )
            return false;
          String name = ie.getName().toLowerCase();
          if( name.endsWith("status") )
            return false;
          if( name.endsWith("state") )
            return false;
          
          return true;
        }
        
      });
      attList.addErrorListener(MainPanel.errWin);
      attList.addSetErrorListener(ErrorPopup.getInstance());
      try {
        attList.add(device.getName()+"/*");
      } catch(ConnectionException e) {}
    
      sl = new ScalarListViewer();
      sl.setModel(attList);
      sp = new JScrollPane(sl);
      splitPane.setRightComponent(sp);
      splitPane.validate();
      if( sp.getViewport().getPreferredSize().height>400 )
        sp.getViewport().setPreferredSize(new Dimension(0,400));
      attList.startRefresher();
      
    }
        
  }
  
  public void addCommand(String cmdName,String text) {
    
    try {
      
      VoidVoidCommand cmd = (VoidVoidCommand)cmdList.add(device.getName()+"/"+cmdName);
      commands.add(cmd);
      VoidVoidCommandViewer viewer = new VoidVoidCommandViewer();
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.gridx = 0;
      gbc.gridy = cmdPos;
      gbc.fill = GridBagConstraints.HORIZONTAL;
      gbc.insets = new Insets(2,1,1,1);      
      commandPanel.add(viewer,gbc);

      viewer.setModel(cmd);
      viewer.setText(text);
      cmdPos++;

    } catch( Exception e) {      
      System.out.println("Error on " + device.getName()+"/"+cmdName);
      System.out.println("  " + e.getMessage());
    }
        
  }
  
  public void addButton(Runnable r,String text) {
    
      final Runnable _r = r;      
      JButton btn = new JButton(text);
      btn.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          _r.run();
        }
      });
      downPanel.add(btn,0);
    
  }
  
  public void addMode(String cmdName,String text) {
    
    try {
      
      VoidVoidCommand cmd = (VoidVoidCommand)cmdList.add(device.getName()+"/"+cmdName);
      modes.add(cmd);
      VoidVoidCommandViewer viewer = new VoidVoidCommandViewer();
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.gridx = 0;      
      gbc.gridy = modePos; 
      gbc.fill = GridBagConstraints.HORIZONTAL;
      gbc.insets = new Insets(2,1,1,1);      
      modePanel.add(viewer,gbc);
      viewer.setModel(cmd);
      viewer.setText(text);
      modePos++;

    } catch( Exception e) {      
      System.out.println("Error on " + device.getName()+"/"+cmdName);
      System.out.println("  " + e.getMessage());
    }
    
  }
  
  private void addDummyPanel(javax.swing.JPanel panel,int pos) {

      javax.swing.JPanel dummyPanel = new javax.swing.JPanel();
      dummyPanel.setMinimumSize(new java.awt.Dimension(0,0));      
      dummyPanel.setPreferredSize(new java.awt.Dimension(0,0));      
      GridBagConstraints gbc = new GridBagConstraints();
      gbc.gridx = 0;      
      gbc.gridy = pos; 
      gbc.fill = GridBagConstraints.BOTH;
      gbc.weighty = 1.0;
      gbc.insets = new Insets(0,0,0,0);
      panel.add(dummyPanel,gbc);
    
  }
  
  public void display() {
    
    if( initOK ) {
            
      addDummyPanel(commandPanel,cmdPos);
      
      if(modes.isEmpty()) {
        
        rightPanel.remove(modePanel);
        rightPanel.add(commandPanel,BorderLayout.CENTER);
        
      } else {

        addDummyPanel(modePanel,modePos);
        
      }
      
      setTitle(device.getName());
      ATKGraphicsUtils.centerFrameOnScreen(this);
      setVisible(true);
      
    }

  }
  
  private void exitForm() {

    statusViewer.setModel(null);
    cmdList.removeErrorListener(ErrorPopup.getInstance());
    cmdList.clear();
    if( attList!=null ) {      
      attList.removeErrorListener(MainPanel.errWin);
      attList.removeSetErrorListener(ErrorPopup.getInstance());
      attList.stopRefresher();
      if(sl!=null) sl.setModel(null);
      attList.clear();      
    }
    setVisible(false);
    dispose();
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    splitPane = new javax.swing.JSplitPane();
    upPanel = new javax.swing.JPanel();
    statusPanel = new javax.swing.JPanel();
    rightPanel = new javax.swing.JPanel();
    commandPanel = new javax.swing.JPanel();
    modePanel = new javax.swing.JPanel();
    downPanel = new javax.swing.JPanel();
    panelButton = new javax.swing.JButton();
    dismissButton = new javax.swing.JButton();

    splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

    upPanel.setLayout(new java.awt.BorderLayout());

    statusPanel.setLayout(new java.awt.BorderLayout());
    upPanel.add(statusPanel, java.awt.BorderLayout.CENTER);

    rightPanel.setLayout(new java.awt.BorderLayout());

    commandPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Command"));
    java.awt.GridBagLayout commandPanelLayout = new java.awt.GridBagLayout();
    commandPanelLayout.columnWeights = new double[] {1.0};
    commandPanel.setLayout(commandPanelLayout);
    rightPanel.add(commandPanel, java.awt.BorderLayout.NORTH);

    modePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Mode"));
    java.awt.GridBagLayout modePanelLayout1 = new java.awt.GridBagLayout();
    modePanelLayout1.columnWeights = new double[] {1.0};
    modePanel.setLayout(modePanelLayout1);
    rightPanel.add(modePanel, java.awt.BorderLayout.CENTER);

    upPanel.add(rightPanel, java.awt.BorderLayout.EAST);

    splitPane.setLeftComponent(upPanel);

    getContentPane().add(splitPane, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    panelButton.setText("Panel");
    panelButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        panelButtonActionPerformed(evt);
      }
    });
    downPanel.add(panelButton);

    dismissButton.setText("Dismiss");
    dismissButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        dismissButtonActionPerformed(evt);
      }
    });
    downPanel.add(dismissButton);

    getContentPane().add(downPanel, java.awt.BorderLayout.SOUTH);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void dismissButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dismissButtonActionPerformed
    // TODO add your handling code here:
    exitForm();
  }//GEN-LAST:event_dismissButtonActionPerformed

  private void panelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_panelButtonActionPerformed
 
    if( splitPane.getRightComponent()==null ) {
      addPanel();
      panelButton.setText("Hide panel");
      splitPane.setRightComponent(sp);    
      pack();      
    } else {
      panelButton.setText("Show panel");
      splitPane.setRightComponent(null);    
      pack();   
    }
    
  }//GEN-LAST:event_panelButtonActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {

    /* Create and display the form */    
    PanelCore tp = new PanelCore("taco:sr/rf-drivloop/tra1");
    
    tp.addCommand("DevOpen","Open");
    tp.addCommand("DevClose","Close");
    tp.addCommand("DevReset","Reset");
            
    tp.addMode("DevSetModeExt","DSP (SYRF)");
    tp.addMode("DevSetModePin","Power In");
    tp.addMode("DevSetModePout","Power Out");
    tp.addMode("DevSetModeExtSrc","Ext. SRC");
            
    tp.display();
                
  }
  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel commandPanel;
  private javax.swing.JButton dismissButton;
  private javax.swing.JPanel downPanel;
  private javax.swing.JPanel modePanel;
  private javax.swing.JButton panelButton;
  private javax.swing.JPanel rightPanel;
  private javax.swing.JSplitPane splitPane;
  private javax.swing.JPanel statusPanel;
  private javax.swing.JPanel upPanel;
  // End of variables declaration//GEN-END:variables
}
