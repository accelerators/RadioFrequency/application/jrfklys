/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class HVPSTangoPanel extends PanelCore {

  public HVPSTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("On","On");
    addCommand("Standby","Standby");
    addCommand("Off","Off");
    addCommand("Reset","Reset");
    
    addMode("SetModeAnodeOnly","Anode Only");
    addMode("SetModeOperation","Operation");
    addMode("SetModeHVPSOnly","HVPS Only");
                        
    display();

  }  

}
