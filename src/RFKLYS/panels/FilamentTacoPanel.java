/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class FilamentTacoPanel extends PanelCore {
  
  public FilamentTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOn","On");
    addCommand("DevStandby","Standby");
    addCommand("DevOff","Off");
    addCommand("DevReset","Reset");
            
    display();
                        
  }  

}
