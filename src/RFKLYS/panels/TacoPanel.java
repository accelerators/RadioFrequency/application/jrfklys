package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class TacoPanel extends PanelCore {
  
  public TacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOn","On");
    addCommand("DevOff","Off");
    addCommand("DevReset","Reset");
                        
    display();

  }  
  
}
