package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class PhaseRegulationTacoPanel extends PanelCore {

  public PhaseRegulationTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOpen","Open");
    addCommand("DevClose","Close");
    addCommand("DevReset","Reset");
                        
    display();

  }  
  
}
