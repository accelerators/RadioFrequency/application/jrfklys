/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

import static RFKLYS.MainPanel.errWin;
import RFKLYS.RFKLYSConst;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributePolledList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DeviceFactory;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.attribute.StringSpectrum;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.SimpleStringSpectrumViewer;
import fr.esrf.tangoatk.widget.attribute.StatusViewer;
import fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 *
 * @author pons
 */
public class DetailedPLCPanel extends javax.swing.JFrame {

  private final String[] labels = {"PLC ITLK ",
                                   "PLC KLYS ",
                                   "PLC LOAD",
                                   "PSW HIS  ",
                                   "CRWB HIS ",
                                   "ARCS HIS "};

  private AttributeList attList;
  private CommandList cmdList;
  private String masterDevName;
  private int klysIdx;

  private static DetailedPLCPanel tra1Panel = null;  
  private static DetailedPLCPanel tra2Panel = null;  
  private static DetailedPLCPanel tra3Panel = null;  
  
  public static void showPLCPanel(String klysName) {
        
    // Get kystron index    
    if( klysName.toLowerCase().endsWith("tra1") ) {
      if(tra1Panel==null)
        tra1Panel = new DetailedPLCPanel(klysName,0);
      tra1Panel.setVisible(true);      
    } else if ( klysName.toLowerCase().endsWith("tra2") ) {
      if(tra2Panel==null)
        tra2Panel = new DetailedPLCPanel(klysName,1);
      tra2Panel.setVisible(true);            
    } else if ( klysName.toLowerCase().endsWith("tra3") ) {
      if(tra3Panel==null)
        tra3Panel = new DetailedPLCPanel(klysName,2);
      tra3Panel.setVisible(true);      
    } else {
      JOptionPane.showMessageDialog(null, "PLCPanel(): Invalid input argument: " + klysName);
      return;
    }
    
  }

  /**
   * Creates new form PLCPanel
   */
  public DetailedPLCPanel(String devName,int klysIdx) {

    masterDevName = devName;
    this.klysIdx = klysIdx;
    
    attList = new AttributePolledList();
    attList.stopRefresher();
    cmdList = new CommandList();
    attList.addErrorListener(errWin);
    cmdList.addErrorListener(ErrorPopup.getInstance());

    initComponents();
        
    // Load PLC synoptic
    InputStream jdFileInStream = this.getClass().getResourceAsStream(RFKLYSConst.PLC_SYNOPTIC_FILE_NAME[klysIdx]);

    if (jdFileInStream == null) {
      JOptionPane.showMessageDialog(
              null, "Failed to get the inputStream for the synoptic file resource : " + RFKLYSConst.PLC_SYNOPTIC_FILE_NAME[klysIdx] + " \n\n",
              "Resource error",
              JOptionPane.ERROR_MESSAGE);
      return;
    }
    
    
    InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
    try {
      plcSynoptic.setErrorHistoryWindow(errWin);
      plcSynoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
      plcSynoptic.loadSynopticFromStream(inStrReader);
      plcSynoptic.computePreferredSize();
    } catch (IOException ioex) {
      JOptionPane.showMessageDialog(
              null, "Cannot find load the synoptic input stream reader.\n" + ioex,
              "Resource access failed",
              JOptionPane.ERROR_MESSAGE);
    }
        
    // Initialise TAB pane
    for(int i=0;i<RFKLYSConst.PLC_NAMES[klysIdx].length;i++)
      tabPane.add(createPLCPanel(i),labels[i]);
    
    attList.setRefreshInterval(1000);
    setTitle("RF PLC [SRRF" + (klysIdx+1) + "]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
        
  }
  
  public void setVisible(boolean visible) {
    if(visible)
      attList.startRefresher();
    else
      attList.stopRefresher();    
    super.setVisible(visible);
  }
  
  private JPanel createPLCPanel(int idx) {
    
    JPanel ret = new JPanel();
    ret.setLayout(new BorderLayout());
    String plcName = RFKLYSConst.PLC_NAMES[klysIdx][idx];
    
    // History & Status
    try {

      JSplitPane sp = new JSplitPane();
      sp.setOrientation(JSplitPane.VERTICAL_SPLIT);
      
      StringScalar status = (StringScalar)attList.add(plcName+"/Status");
      StatusViewer statusViewer = new StatusViewer();
      statusViewer.setModel(status);
      statusViewer.setPreferredSize(new Dimension(300,300));
      sp.setTopComponent(statusViewer);
      

      StringSpectrum history = (StringSpectrum)attList.add(plcName+"/InterlockHistory");        
      SimpleStringSpectrumViewer hViewer = new SimpleStringSpectrumViewer();
      hViewer.setModel(history);
      hViewer.setPreferredSize(new Dimension(300,200));
      sp.setBottomComponent(hViewer);

      ret.add(sp,BorderLayout.CENTER);
      
    } catch(ConnectionException e) {}
    
    // Reset
    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    try {
      VoidVoidCommand reset = (VoidVoidCommand)cmdList.add(plcName+"/Reset");
      VoidVoidCommandViewer resetCmd = new VoidVoidCommandViewer();
      resetCmd.setModel(reset);
      resetCmd.setText("Reset " + labels[idx].trim());
      btnPanel.add(resetCmd);
    } catch(ConnectionException e) {}
    ret.add(btnPanel,BorderLayout.SOUTH);
    
    return ret;
    
  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    btnPanel = new javax.swing.JPanel();
    resetAll = new javax.swing.JButton();
    dismissButton = new javax.swing.JButton();
    plcSynoptic = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
    tabPane = new javax.swing.JTabbedPane();

    btnPanel.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

    resetAll.setText("Reset All");
    resetAll.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        resetAllActionPerformed(evt);
      }
    });
    btnPanel.add(resetAll);

    dismissButton.setText("Dismiss");
    dismissButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        dismissButtonActionPerformed(evt);
      }
    });
    btnPanel.add(dismissButton);

    getContentPane().add(btnPanel, java.awt.BorderLayout.SOUTH);
    getContentPane().add(plcSynoptic, java.awt.BorderLayout.NORTH);
    getContentPane().add(tabPane, java.awt.BorderLayout.CENTER);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void dismissButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dismissButtonActionPerformed
    setVisible(false);
  }//GEN-LAST:event_dismissButtonActionPerformed

  private void resetAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetAllActionPerformed

    try {
      DeviceProxy ds = DeviceFactory.getInstance().getDevice(masterDevName);
      ds.command_inout("Reset");
    } catch(DevFailed|ConnectionException e) {
      ErrorPane.showErrorMessage(this, masterDevName, e);
    }
    
  }//GEN-LAST:event_resetAllActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel btnPanel;
  private javax.swing.JButton dismissButton;
  private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer plcSynoptic;
  private javax.swing.JButton resetAll;
  private javax.swing.JTabbedPane tabPane;
  // End of variables declaration//GEN-END:variables
}
