package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class DriverLoopTacoPanel extends PanelCore {
  
  public DriverLoopTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOpen","Open");
    addCommand("DevClose","Close");
    addCommand("DevReset","Reset");
            
    addMode("DevSetModeExt","DSP (SYRF)");
    addMode("DevSetModePin","Power In");
    addMode("DevSetModePout","Power Out");
    addMode("DevSetModeExtSrc","Ext. SRC");
            
    display();
                        
  }  
  
}
