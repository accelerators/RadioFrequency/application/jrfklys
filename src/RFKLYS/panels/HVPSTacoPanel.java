/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class HVPSTacoPanel extends PanelCore {

  public HVPSTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOn","On");
    addCommand("DevStandby","Standby");
    addCommand("DevOff","Off");
    addCommand("DevReset","Reset");
    
    addMode("DevSetModeAnodeOnly","Anode Only");
    addMode("DevSetModeOperation","Operation");
    addMode("DevSetModeHVPSOnly","HVPS Only");
                        
    display();

  }  

}
