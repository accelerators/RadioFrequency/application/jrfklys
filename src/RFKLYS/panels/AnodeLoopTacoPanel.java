package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class AnodeLoopTacoPanel extends PanelCore {

  public AnodeLoopTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevOpen","Open");
    addCommand("DevClose","Close");
    addCommand("DevReset","Reset");
    
    addMode("DevSetCurrentMode","Klys Current");
    addMode("DevSet2CavVoltageMode","Cav Voltage");
                        
    display();

  }  

}
