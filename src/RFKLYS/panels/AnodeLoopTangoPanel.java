package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class AnodeLoopTangoPanel extends PanelCore {

  public AnodeLoopTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("Open","Open");
    addCommand("Close","Close");
    addCommand("Reset","Reset");
    
    addMode("KlystronCurrentMode","Klys Current");
    addMode("CavVoltageMode","Cav Voltage");
                        
    display();

  }  

}
