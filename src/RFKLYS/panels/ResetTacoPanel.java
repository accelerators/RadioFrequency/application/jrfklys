package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class ResetTacoPanel extends PanelCore {
  
  public ResetTacoPanel(String devName) {
   
    super(devName);
    
    addCommand("DevReset","Reset");
                        
    display();

  }  

}
