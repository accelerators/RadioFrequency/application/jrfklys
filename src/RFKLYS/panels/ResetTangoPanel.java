package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class ResetTangoPanel extends PanelCore {
  
  public ResetTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("Reset","Reset");
                        
    display();

  }  

}
