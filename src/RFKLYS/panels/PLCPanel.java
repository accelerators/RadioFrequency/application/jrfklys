package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class PLCPanel extends PanelCore {
  
  public PLCPanel(String devName) {

    super(devName,true);
    
    final String _devName = devName;
    addButton(new Runnable() {
      @Override
      public void run() {
        DetailedPLCPanel.showPLCPanel(_devName);
      }      
    },"Detailed Panel");
    
    addCommand("Reset","Reset");
    
                        
    display();

  }  

}
