package RFKLYS.panels;

/**
 *
 * @author pons
 */
public class PhaseRegulationTangoPanel extends PanelCore {

  public PhaseRegulationTangoPanel(String devName) {
   
    super(devName,true);
    
    addCommand("Open","Open");
    addCommand("Close","Close");
    addCommand("Reset","Reset");
                        
    display();

  }  
  
}
