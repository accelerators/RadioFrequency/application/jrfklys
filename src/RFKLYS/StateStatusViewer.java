package RFKLYS;

import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IDevStateScalarListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.IStringScalarListener;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.JSmoothLabel;

/**
 * A class to display a DevState viewer containing a status (StringScalar)
 *
 * @author pons
 */
public class StateStatusViewer extends JSmoothLabel implements IDevStateScalarListener, IStringScalarListener {

  private IDevStateScalar stateModel = null;
  private IStringScalar statusModel = null;
  private String currentState = null;
  private String currentStatus = null;

  public StateStatusViewer() {
    
    currentState = new String(IDevice.UNKNOWN);
    currentStatus = new String(IDevice.UNKNOWN);
    setBackground(ATKConstant.getColor4State(currentState));
    setOpaque(true);
    setText(currentStatus);
    setFont(new java.awt.Font("Dialog", 0, 14));
    
  }
  
  public String getState() {
    return currentState;
  }

  public void setStateModel(IDevStateScalar stateAtt) {
    
    if (stateModel != null) {
      stateModel.removeDevStateScalarListener(this);
      stateModel = null;
      setToolTipText(null);
    }

    if (stateAtt == null) {
      return;
    }

    stateModel = stateAtt;
    stateAtt.addDevStateScalarListener(this);

    setToolTipText(stateAtt.getName());
    setCurrentState(stateAtt.getDeviceValue());
    
  }

  public void setStatusModel(IStringScalar statusAtt) {
    
    if (statusModel != null) {
      statusModel.removeStringScalarListener(this);
      statusModel = null;
    }

    if (statusAtt == null) {
      return;
    }

    statusModel = statusAtt;
    statusModel.addStringScalarListener(this);
    currentStatus = statusModel.getStringDeviceValue();
    setText(currentStatus);
    
  }
  
  public void clearModel() {
    
    setStateModel(null);
    setStatusModel(null);
    
  }

  public void devStateScalarChange(fr.esrf.tangoatk.core.DevStateScalarEvent devStateScalarEvent) {
    setCurrentState(devStateScalarEvent.getValue());
  }

  public void errorChange(fr.esrf.tangoatk.core.ErrorEvent errorEvent) {
    
    if (errorEvent.getSource() == statusModel) {
      if (!currentStatus.equals(IDevice.UNKNOWN)) {
        currentStatus = new String(IDevice.UNKNOWN);
        setText(currentStatus);
      }
    }

    if (errorEvent.getSource() == stateModel) {
      setCurrentState(IDevice.UNKNOWN);
    }
    
  }

  public void stateChange(fr.esrf.tangoatk.core.AttributeStateEvent attributeStateEvent) {// quality factor change for status string attribute : do nothing
  }

  public void stringScalarChange(fr.esrf.tangoatk.core.StringScalarEvent stringScalarEvent) {
    String val;

    val = stringScalarEvent.getValue();

    if (!val.equals(currentStatus)) {
      currentStatus = new String(val);
      setText(currentStatus);
    }
  }

  private void setCurrentState(String stateStr) {
    if (!currentState.equals(stateStr)) {
      currentState = new String(stateStr);
      setBackground(ATKConstant.getColor4State(currentState));
    }
  }
}
