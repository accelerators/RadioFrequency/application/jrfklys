/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RFKLYS;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author pons
 */
public class WaveformFrame extends JFrame implements ActionListener {
  
  private WaveformPanel wPanel;
  private JButton dismissBtn;

  public WaveformFrame(String devName) {

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitPanel();
      }
    });
    
    JPanel innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());
   
    wPanel = new WaveformPanel(devName,MainPanel.errWin);
    innerPanel.add(wPanel,BorderLayout.CENTER);
    
    JPanel downPanel = new JPanel();
    downPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    innerPanel.add(downPanel,BorderLayout.SOUTH);

    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    downPanel.add(dismissBtn);
      
    setContentPane(innerPanel);
    setTitle("Waveform [" + devName + "]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);

  }

  private void exitPanel() {
    wPanel.clearModel();
    dispose();
  }
  
  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if ( src==dismissBtn ) {

      dispose();
      setVisible(false);

    }

  }

  
}
